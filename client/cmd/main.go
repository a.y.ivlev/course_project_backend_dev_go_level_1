package main

import (
	"Course_project_Backend_dev_Go_level_1/client/internal/handlers"
	"net/http"
)


func main() {
	http.HandleFunc("/", handlers.HomePage)
	http.ListenAndServe(":8020", nil)
}
